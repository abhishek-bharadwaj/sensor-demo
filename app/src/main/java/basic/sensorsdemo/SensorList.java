package basic.sensorsdemo;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class SensorList extends AppCompatActivity implements AdapterView.OnItemClickListener {

    public static final String SENSOR_NAME = "sensor_name";
    public static final String SENSOR_TYPE = "sensor_type";

    List<Sensor> sensors;

    ListView lvSensorsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor_list);

        lvSensorsList = (ListView) findViewById(R.id.lv_sensors_list);
        lvSensorsList.setOnItemClickListener(this);
        setUpSensorsList();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent intent = new Intent(this, SensorData.class);
        intent.putExtra(SENSOR_NAME, sensors.get(i).getName());
        intent.putExtra(SENSOR_TYPE, sensors.get(i).getType());
        startActivity(intent);
    }

    private void setUpSensorsList() {
        SensorManager sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensors = sensorManager.getSensorList(Sensor.TYPE_ALL);
        List<String> sensorsName = new ArrayList<>();
        for (Sensor sensor:sensors) {
            sensorsName.add(sensor.getName());
        }
        lvSensorsList.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, sensorsName));
    }
}
