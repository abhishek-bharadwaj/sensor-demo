package basic.sensorsdemo;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class SensorData extends AppCompatActivity implements SensorEventListener {

    int sensorType;

    ListView lvSensorData;
    TextView tvSensorName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor_data);

        getViewReference();
        getIntentDataAndSetView();
        registerSensor();
    }

    private void getViewReference() {
        lvSensorData = (ListView) findViewById(R.id.lv_sensor_data);
        tvSensorName = (TextView) findViewById(R.id.tv_sensor_name);
    }

    private void getIntentDataAndSetView() {
        String sensorName = getIntent().getStringExtra(SensorList.SENSOR_NAME);
        sensorType = getIntent().getIntExtra(SensorList.SENSOR_TYPE, 0);
        tvSensorName.setText(sensorName);
    }

    private void registerSensor() {
        SensorManager senSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        senSensorManager.registerListener(this, senSensorManager.getDefaultSensor(sensorType),
                SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        Sensor mySensor = sensorEvent.sensor;
        if (mySensor.getType() == sensorType) {
            float[] sensorValues = sensorEvent.values;
            List<String> sensorData = new ArrayList<>();
            for (float value : sensorValues) {
                sensorData.add(value + "");
            }
            lvSensorData.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, sensorData));
        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
